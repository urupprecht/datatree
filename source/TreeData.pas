(*
****************************************************************
* Unit:		      TreeData
* Area:         Main
* Author:	      (C) 2018 Uwe Rupprecht
* Purpose:
* Version:	    1.0.1.0
* Last Change:	26.08.2018 12:06
* History:      -
****************************************************************
* This Source Code Form is subject to the terms of the
* Mozilla Public License, v. 2.0. If a copy of the MPL was not
* distributed with this file, You can obtain one at
* https://mozilla.org/MPL/2.0/.
****************************************************************
*)
unit TreeData;

interface
uses
  {$ifdef FASTMM}
  FastMM4,
  {$endif}
  WinAPI.Windows,
  System.Generics.Collections;

Type
  TDataNodeFlag = (dnfIsRoot);
  TDataNodeFlags = Set Of TDataNodeFlag;

  //Nodes for the Chain within a Datatree.

  PDataNode = ^TDataNode;
  TDataNode = Packed Record
    Parent     : PDataNode; //Pointer to the Parentnode
    Prev,Next  : PDataNode; //Pointer to the Previouse/Next sibling node
    FirstChild,LastChild: PDataNode; //Pointer to the first and last Childnode
  Private
    index      : Cardinal;  //Unique Index of a node
    ChildCount : Cardinal;  //count of the childs of a node
    Level      : Cardinal;  //Treelevel of a node
    flags      : TDataNodeFlags; //Set of Flags for the node

    Data : Record end;  //Placeholder for the Userdata
  public
    function IsAssigned():boolean;inline;  //Checks if the node is valid
    function HasChilds():boolean;inline;   //Checks if a node has childs
    function GetData():Pointer;overload;inline; //Get the Nodedata as pointer
    function GetData<T>():T;overload;Inline;    //Get the Nodedata as Type
    Procedure SetData(pUserdata:pointer);overload; //Set the Nodedata as Pointer
    Procedure SetData<T>(Puserdata:T);Overload; //Set the Nodedata as Type
    Procedure SetData(const PUserdata:IInterface);Overload; //Set the Userdata as Interface

    function GetIndex:Cardinal; //Get the index of a node
    function GetLevel:Cardinal; //Get the Level of a node;
    function GetFlags: TDataNodeFlags; //Get the set of Flags, set in the node
  End;

  TDataTree = Class;

  //Event, that is called, before a node and the userdata is freed.
  //Needed to finalze the userdata (Unicodestrings for example)
  TDTFreeNode = Procedure(source:TDataTree;var Node:PDataNode) of Object;
  //Event, that is called on each node, when the user calls the Walk-Method
  //to walk throug the complete chain.
  TDTWalkNode = Procedure(source:TDataTree;Var node:PDataNode;var stopwalking:boolean) of Object;
  //Event, that is called, when we search for a node depending on the
  //userdefined Data. If the result is true, the nodedata matches the search criteria
  //and the search will end;
  TDTCompareNodeData = function(Search:Pointer;NodeData:Pointer):boolean of Object;

  TNodeEnumerator = Class
    private
       froot,fcurrent : PDataNode;
       function GetCurrent : PDataNode;
    public
      Constructor Create(var root:PDataNode);
      function MoveNext:boolean;
      procedure Reset;

      Property Current : PDataNode read GetCurrent;
  End;

  //Mainclass to work with a Datatree
  TDataTree = Class(TObject)
    private
       fNodeDataSize: Cardinal;   //Size of the Userdata
       fNodeCount : Cardinal;     //Total count of nodes within the tree

       froot : PDataNode;         //Root-Node of the Tree

       fonFreeNode : TDTFreeNode;
       fonWalkNode : TDTWalkNode;
       fonCompareNodeData : TDTCompareNodeData;

       //Initializing the Root node
       Procedure InitRootNode(Oldsize:Cardinal=0);
       //Creates a new Dataanode
       function MakeNewNode(parent:PDataNode=NIL):PDataNode;

    protected
       //Cleans up the complete Chain and Free the nodes
       Procedure CleanUpChain;

    public

       Constructor Create(NodeDataSize:Cardinal);
       Destructor Destroy;override;

       //Calls the CleanUpChain method to clear the chain and free the nodes
       Procedure Clear;
       //Add a new node
       function AddChild(Parent:PDataNode;UserData:Pointer=NIL):PDataNode;
       //Get the userdata of a given node
       function GetNodeData(Node:PDataNode):Pointer;
       //Delete a node and his childs
       function DeleteNode(Node:PDataNode):boolean;
       //Navigation throug the nodes

       //Navigation on the same level of the given node
       //If Node = NIL then the Childs of the rootnode are used
       function GetFirst(Node:PDataNode=NIL):PDataNode;
       function GetLast(Node:PDataNode=NIL):PDataNode;
       function GetNext(Node:PDataNode=NIL):PDataNode;
       function GetPrev(Node:PDataNode=NIL):PDataNode;

       //Navigation throug the Childnodes of a given node
       //if no node given, the rootnode is used
       function GetFirstChild(Node:PDataNode=NIL):PDataNode;
       function GetLastChild(Node:PDataNode=NIL):PDataNode;

       //Search for a Node by Index
       function SearchNode(Index:Cardinal):PDataNode;overload;
       //Search for a Node using the userdefined Data
       function SearchNode(SearchData:Pointer):PDataNode;overload;

       //Enumerator/Enumerable
       Function GetEnumerator:TNodeEnumerator;


       //Step to the complete Tree and Call the event
       Procedure WalkTree;

       Property RootNode : PDataNode read froot;

    published
       Property TotalNodes : Cardinal read fNodeCount;
       Property NodeDataSize: Cardinal read fNodeDatasize;

       Property onFreeNode : TDTFreeNode read fonFreeNode write fonFreeNode;
       Property onWalkNode : TDTWalkNode read fonWalkNode write fonWalkNode;
       Property onCompareNodeData : TDTCompareNodeData read fonCompareNodeData write fonCompareNodeData;

  End;

implementation
uses
  System.SysUtils;

const
   //See Virtual String Tree for more Infos
   cDataNodeSize = (SizeOf(TDataNode) + (SizeOf(Pointer) - 1)) and not (SizeOf(Pointer) - 1); // used for node allocation and access to internal data

{$Region 'TDataNode'}
function TDataNode.IsAssigned:boolean;
begin
  result := (@self <> NIL);
end;

function TDataNode.HasChilds:boolean;
begin
  result := (Childcount > 0);
end;

function TDataNode.GetData:Pointer;
begin
 result := @self.Data;
end;

function TDataNode.GetData<T>:T;
begin
  result := T(Pointer((PByte(@(Self.Data))))^);
end;

procedure TDataNode.SetData(pUserdata: Pointer);
var
  NodeData : PPointer;

begin
  NodeData := PPointer(@self.data);
  nodedata^ := pUserdata;
end;

procedure TDataNode.SetData<T>(Puserdata: T);
begin
  T(Pointer((PByte(@(Self.Data))))^) := pUserData;
end;

procedure TDataNode.SetData(const PUserdata: IInterface);
begin
  puserdata._AddRef;
  SetData(Pointer(pUserdata));
end;

function TDataNode.GetIndex:Cardinal;
begin
  result := Index;
end;

function TDataNode.GetLevel:Cardinal;
begin
  result := Level;
end;

function TDataNode.GetFlags:TDataNodeFlags;
begin
  result := Flags;
end;
{$ENDREGION}

{$Region 'TDataTree'}
Constructor TDataTree.Create(NodeDataSize: Cardinal);
begin
  froot := NIL;
  fNodeDataSize := NodeDataSize;
  fNodeCount := 0;
end;

Destructor TDataTree.Destroy;
begin
  CleanUpChain;
  fnodeDataSize := 0;
  fNodeCount := 0;
  inherited Destroy;
end;

Procedure TDataTree.CleanUpChain;

  Procedure FreeNode(var node:PDataNode);
  var
    size : Cardinal;
  begin
    size := fNodeDataSize+cDataNodeSize;
    //Rootnode has no data, so no  need to call then event
    if (Assigned(fonFreeNode)) and (node <> froot) then
      fonFreeNode(self,node);
    FreeMem(node,size);
    Node := NIL;
  end;

  Procedure LoopNodes(base : PDataNode);
  var
    work : PDataNode;
  begin
    while (base <> NIL) do
    begin
      work := base;
      base := base^.Next;
      if (work^.ChildCount > 0) then
        LoopNodes(work^.FirstChild);
      FreeNode(work);
    end;
  end;

begin
  if (froot <> NIL) then
  begin
    if (froot^.ChildCount > 0) then
    begin
      LoopNodes(froot^.FirstChild);
      FreeNode(Froot);
    end;
  end;
end;

function TDataTree.MakeNewNode(Parent:PDataNode):PDataNode;
var
  size : Cardinal;

begin
  result := NIL;
  if (fNodeDataSize > 0) then
  begin
    size := cDataNodeSize + fNodeDataSize; //Internal Size+UserDataSize in Bytes
    result := AllocMem(size);
    Inc(fNodeCount);
    result^.index := fNodeCount;
    result^.ChildCount := 0;
    result^.flags := [];
    if (parent <> NIL) then
    begin
      result^.Level := parent^.Level+1;
      result^.Parent := parent;
      inc(parent^.ChildCount);
    end;
  end;
end;



Procedure TDataTree.InitRootNode(Oldsize:cardinal);
var
  size : Cardinal;

begin
  if (froot = NIL) then
    froot := MakeNewNode
  else
  begin
    size := cDataNodeSize + fNodeDataSize; //Internal Size+UserDataSize in Bytes
    reallocmem(froot,size);
    ZeroMemory(PByte(froot)+OldSize,Size-OldSize);
  end;
  fNodeCount := 0;
  froot^.index := 1;
  froot^.ChildCount := 0;
  froot^.Level := 0;
  froot^.Prev := froot;
  froot^.Next := froot;
  froot^.Parent := NIL;
end;

function TDataTree.AddChild(Parent: PDataNode; UserData: Pointer = nil):PDataNode;
var
  Child : PDataNode;
begin
  if (Parent = NIL) then
  begin
    if (froot = NIL) then
      InitRootNode;
    Parent := froot;
  end;
  Child := MakeNewNode(parent);
  if (parent = froot) then
    include(child^.flags,dnfIsRoot);
  child^.Prev := Parent^.LastChild;
  if (Assigned(parent^.LastChild)) then
    parent^.LastChild^.Next := child;
  parent^.LastChild := Child;
  if (not Assigned(parent^.FirstChild)) then
    parent^.FirstChild := child;
  Inc(parent^.ChildCount);
  child^.SetData(Userdata);
  child^.Level := Child^.parent^.Level+1;
  result := Child;
end;


function TDataTree.GetNodeData(Node: PDataNode):Pointer;
begin
  result := NIL;
  if (node <> NIL) and (fNodeDatasize > 0) and (node <> froot) then
    result := @node.Data;
end;

function TDataTree.DeleteNode(Node: PDataNode):boolean;
var
  run : PDataNode;
  size : Cardinal;

  Procedure DeleteChildren(node:PDataNode);
  var
    mark,run : PDataNode;

  begin
    mark := node^.LastChild;
    run := node^.LastChild^.Prev;
    while(run <> NIL) do
    begin
      FreeMem(mark,size);
      Mark := NIL;
      mark := run;
      run := run^.Prev;
    end;
    if (mark <> NIL) then
    begin
      freemem(mark,size);
      mark := NIL;
    end;
  end;

begin
  result := false;
  if (node <> froot) then
  begin
    size := cDataNodeSize+fnodedatasize;
    if (node^.ChildCount > 0) then
      DeleteChildren(node);
    //eleminate node from chain;
    run := node^.Prev;
    run^.Next := node^.Next;
    run^.next^.Prev := run;

    freeMem(node,size);
    Node := NIL;
    result := true;
  end;
end;

function TDataTree.GetEnumerator: TNodeEnumerator;
begin
  result := TNodeEnumerator.Create(froot);
end;

function TDataTree.GetFirst(Node:PDataNode=NIL):PDataNode;
begin
  result := NIL;
  if (node = NIL) then
  begin
    if (froot = nil) then
      Raise Exception.Create('No Data available !');
    if (froot^.ChildCount > 0) then
      result := froot^.FirstChild;
  end
  else
  begin
    if (node^.Parent <> NIL) then
      result := node^.parent^.FirstChild;
  end;
end;

function TDataTree.GetLast(Node:PDataNode=NIL):PDataNode;
begin
  result := NIL;
  if (node = NIL) then
  begin
    if (froot^.ChildCount > 0) then
      result := froot^.LastChild;
  end
  else
  begin
    if (node^.parent <> NIL) then
      result := node^.Parent^.LastChild;
  end;
end;

function TDataTree.GetNext(Node:PDataNode=NIL):PDataNode;
begin
  result := NIL;
  if (node <> NIL) then
    result := node^.Next
end;

function TDataTree.GetPrev(Node:PDataNode=NIL):PDataNode;
begin
  result := NIL;
  if (node <> NIL) then
    result := node^.Prev;
end;

//Navigation throug the Childnodes of a given node
//if no node given, the rootnode is used
function TDataTree.GetFirstChild(Node:PDataNode=NIL):PDataNode;
begin
  result := NIL;
  if (node = NIL) then
  begin
    if (froot <> NIL) and (froot^.ChildCount > 0) then
      result := froot^.FirstChild;
  end
  else
  begin
    if (node^.ChildCount > 0) then
      result := node^.FirstChild;
  end;
end;

function TDataTree.GetLastChild(Node:PDataNode=NIL):PDataNode;
begin
  result := nil;
  if (node = NIL) then
  begin
    if (froot <> NIL) and (froot^.ChildCount > 0) then
      result := froot^.LastChild;
  end
  else
  begin
    if (node^.ChildCount > 0) then
      result := node^.LastChild;
  end;
end;

Procedure TDataTree.Clear;
begin
  CleanUpChain;
end;


procedure TDataTree.WalkTree;

  Procedure LoopChilds(base:PDataNode;var stopwalk:boolean);
  var
    child : PDataNode;

  begin
    child := base^.FirstChild;
    while (child <> NIL) and (not stopwalk) do
    begin
      fonWalkNode(self,child,stopwalk);
      if (not stopwalk) and (child^.ChildCount > 0) then
        LoopChilds(child,stopwalk);
      if (not stopwalk) then
        child := child^.Next;
    end;
  end;

var
  node : PDataNode;
  Stopwalk : boolean;

begin
  if (Assigned(fonWalkNode)) then
  begin
    StopWalk := False;
    if (froot <> NIL) then
    begin
      node := froot^.FirstChild;
      while (node <> NIL) and (not StopWalk) do
      begin
        fonWalkNode(self,node,Stopwalk);
        if (not Stopwalk) and (node^.ChildCount > 0) then
          LoopChilds(node,stopwalk);
        if (not StopWalk) then
          node := node^.Next;
      end;
    end
    else
    begin
      node := NIL;
      fonwalknode(self,node,stopwalk);
    end;
  end;
end;

function TDataTree.SearchNode(Index: Cardinal):PDataNode;

  function LoopChilds(Base:PDataNode;Index:Cardinal):PDataNode;
  var
    child : PDataNode;

  begin
    result := NIL;
    child := base^.FirstChild;
    while (child <> NIL) and (result = NIL) do
    begin
      if (child^.index = index) then
        result := child
      else
      begin
        if (child^.ChildCount > 0) then
          result := LoopChilds(child,index);
        if (result = NIL) then
          child := child^.Next;
      end;
    end;
  end;

var
  node : PDataNode;

begin
  result := NIL;
  node := froot^.FirstChild;
  while (node <> NIL) and (result = NIL) do
  begin
    if (node^.index = index) then
      result := node
    else
    begin
      if (node^.ChildCount > 0) then
        result := LoopChilds(node,index);
      if (result = NIL) then
        node := node^.Next;
    end;
  end;
end;

function TDataTree.SearchNode(Searchdata: Pointer):PDataNode;

  function LoopChilds(Base:PDataNode;Searchdata:Pointer):PDataNode;
  var
    child : PDataNode;

  begin
    result := NIL;
    child := base^.FirstChild;
    while (child <> NIL) and (result = NIL) do
    begin
      if (fonCompareNodeData(Searchdata,child^.GetData)) then
        result := child
      else
      begin
        if (child^.ChildCount > 0) then
          result := LoopChilds(child,Searchdata);
        if (result = NIL) then
          child := child^.Next;
      end;
    end;
  end;

var
  node : PDataNode;

begin
  result := NIL;
  if not assigned(fonCompareNodeData) then exit;
  node := froot^.FirstChild;
  while (node <> NIL) and (result = NIL) do
  begin
    if (fonCompareNodeData(Searchdata,node^.GetData)) then
      result := node
    else
    begin
      if (node^.ChildCount > 0) then
        result := LoopChilds(node,searchdata);
      if (result = NIL) then
        node := node^.Next;
    end;
  end;
end;

{$ENDREGION}


{ TNodeEnumerator }

constructor TNodeEnumerator.Create(var root: PDataNode);
begin
  froot := root;
  fcurrent := NIL;
end;

function TNodeEnumerator.GetCurrent: PDataNode;
begin
  result := NIL;
  if (fCurrent <> NIL) and (fcurrent <> froot) then
    result := fcurrent;
end;

function TNodeEnumerator.MoveNext: boolean;
var
  done : boolean;
begin
  if (fcurrent = NIL) then
  begin
    if (froot <> NIL) and (froot^.ChildCount > 0) then
      fcurrent := froot^.FirstChild;
  end
  else
  begin
    //Level up
    if (fcurrent^.ChildCount > 0) then
    begin
      fcurrent := fcurrent^.FirstChild;
    end
    else
    begin
      //Forward
      if (fcurrent^.Next <> NIL) then
        fcurrent := fcurrent^.Next
      else
      begin
        if (fcurrent = froot^.LastChild) then
          fcurrent := NIL
        else
        begin
          if (fcurrent^.parent^.Next <> NIL) then
            fcurrent := fcurrent^.Parent^.Next
          else
            fcurrent := fcurrent^.parent^.Parent^.Next;
        end;
      end;
    end;
  end;
  result := (Fcurrent <> NIL) and (FCurrent <> froot);
end;

procedure TNodeEnumerator.Reset;
begin
  fcurrent := NIL;
end;

end.
