# Datatree

Datatree is a class, to handle custom data within a tree based structure.

To do so, the data is embedded with in a chain of pointers. But don't worry, all the handling of the pointers are done internally.

The interface very similar to the one you might know from a component, called "VirtualStringTree". 

# History

V1.0.0.0		Inital release

V1.0.1.0		Added Enumerator for easier handling with
				for in Loop
				
				


