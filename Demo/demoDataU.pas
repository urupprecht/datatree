unit demoDataU;

interface

Type
  //Just the type of our demo data
  PDemoData = ^TDemodata;
  TDemodata = Record
                AInteger : integer;
                AString : string;
                AFloat  : double;
  End;
implementation

end.
