program demo;

uses
  Vcl.Forms,
  mainU in 'mainU.pas' {Form1},
  TreeData in '..\source\TreeData.pas',
  demoDataU in 'demoDataU.pas',
  GenericTreeData in '..\source\GenericTreeData.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Datatree Demo';
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
