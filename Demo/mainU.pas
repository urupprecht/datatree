unit mainU;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,DemoDataU,TreeData, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    mlog: TMemo;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    //Our Datatree to handle the data
    Tree : TDataTree;

    //The simple demo to walk throug all the nodes of the tree
    //here it is used, to output the nodes and there data
    Procedure OutputWalker(source:TDataTree;Var node:PDataNode;var stopwalking:boolean);
    //This has to be done, if you use Unicodestrings within your
    //Data, so the string can be finalized
    Procedure onFreeNode(Source:TDataTree;var node : PDataNode);


    //Some helper for output
    procedure ClearLog;
    Procedure LogMsg(const amsg:string);
    procedure LogNodeData(node:PDatanode);
  end;

var
  Form1: TForm1;

implementation
uses
  System.Math;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  data : PDemodata;
  node,Base : PDataNode;
  i,a : integer;

begin
  tree.Clear;
  Randomize;
  for a := 1 to 10 do
  begin
    base := tree.AddChild(NIL);
    data := tree.GetNodeData(base);
    data^.AInteger := Random(100);
    Data^.AString := 'Basenode';
    Data^.AFloat := RandG(10.0,0.3);
    for I := 1 to 10 do
    begin
      node := tree.AddChild(base);
      data := tree.GetNodeData(node);
      data^.AInteger := 100+random(1000);
      data^.AString := 'Childnode';
      data^.AFloat := RandG(10.0,0.3);
    end;
  end;
  LogMsg('Initialized !');
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  tree.WalkTree;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  ClearLog;
end;

procedure TForm1.ClearLog;
begin
  mlog.Lines.Clear;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Tree := TDataTree.Create(sizeof(TDemoData));
  tree.onFreeNode := onFreeNode;
  tree.onWalkNode := outputWalker;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  Tree.Free;
end;

procedure TForm1.LogMsg(const amsg: string);
begin
  mlog.Lines.Append(amsg);
end;

procedure TForm1.LogNodeData(node: PDatanode);
var
  data : PDemodata;
  spaces : string;
begin
  data := tree.GetNodeData(node);
  mlog.Lines.Append('Node Level: '+IntToStr(node^.GetLevel));
  mlog.Lines.Append('   Integer:'+IntToStr(data^.AInteger));
  mlog.Lines.Append('   String.:'+data^.AString);
  mlog.Lines.Append('   Float..:'+FloatToStr(data^.AFloat));
  finalize(spaces);
end;

procedure TForm1.onFreeNode(Source: TDataTree; var node: PDataNode);
var
  pdata : PDemodata;

begin
  pdata := source.GetNodeData(node);
  finalize(pdata^.AString);
end;

procedure TForm1.OutputWalker(source: TDataTree; var node: PDataNode;
  var stopwalking: boolean);
begin
  if (Node = NIL) then
    logMsg('No nodes to walk throug !')
  else
  begin
    LogNodeData(node);
    Stopwalking := false;
  end;
end;

end.
