object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 351
  ClientWidth = 677
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object mlog: TMemo
    Left = 0
    Top = 152
    Width = 677
    Height = 199
    Align = alBottom
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Button1: TButton
    Left = 8
    Top = 8
    Width = 97
    Height = 25
    Caption = 'Add some Nodes'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 520
    Top = 8
    Width = 149
    Height = 25
    Caption = 'Output Tree'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 594
    Top = 121
    Width = 75
    Height = 25
    Caption = 'Clear Log'
    TabOrder = 3
    OnClick = Button3Click
  end
end
